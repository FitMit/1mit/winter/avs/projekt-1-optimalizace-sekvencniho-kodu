/**
 * @file BatchMandelCalculator.cc
 * @author Matej Kudera <xkuder04@stud.fit.vutbr.cz>
 * @brief Implementation of Mandelbrot calculator that uses SIMD paralelization over small batches
 * @date 17.10.2021
 */

#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

#include <stdlib.h>
#include <stdexcept>

// mm_malloc
#include <immintrin.h>

#include "BatchMandelCalculator.h"

BatchMandelCalculator::BatchMandelCalculator (unsigned matrixBaseSize, unsigned limit) :
	BaseMandelCalculator(matrixBaseSize, limit, "BatchMandelCalculator")
{
	block_height = height;
	block_width = 256;

	data = ((int *) _mm_malloc(height * width * sizeof(int), 64));
	real_values = ((float *) _mm_malloc(block_width * sizeof(float), 64));
	imag_values = ((float *) _mm_malloc(block_width * sizeof(float), 64));

	for (int i = 0; i < width * height; i++)
	{
		data[i] = 0;
	}
}

BatchMandelCalculator::~BatchMandelCalculator() {
	_mm_free(data);
	data = NULL;

	_mm_free(real_values);
	real_values = NULL;

	_mm_free(imag_values);
	imag_values = NULL;
}


int * BatchMandelCalculator::calculateMandelbrot () 
{
	int *pdata = data;
	float *preal_values = real_values;
	float *pimag_values = imag_values;

	// Split the number of rows of the matrix into blocks
  	for (int i_block = 0; i_block < height / block_height; i_block++)
  	{
    	// Split the number of cols of the matrix into blocks
    	for (int j_block = 0; j_block < width / block_width; j_block++)
    	{
			// Go over rows (imaginary numbers)
			for (int i = 0; i < block_height; i++)
			{
				// Calculate global i value and current imag value
				int i_global = i_block * block_height + i;
				float imag = y_start + i_global * dy;

				// Gloabal j value for start of current block
				int global_j_block_start = j_block * block_width;

				// Pointer to start of block
				int *block_start_ptr = pdata + (i_global*width) + global_j_block_start;

				// Go over mandelbrot limit in whole row
				for (int k = 0; k < limit; k++)
				{
					int items_done = 0;

					// Go over cols (real numbers)
					#pragma omp simd simdlen(256) aligned(block_start_ptr, preal_values, pimag_values : 64) reduction(+:items_done) 
					for (int j = 0; j < block_width; j++)
					{
						int j_global = global_j_block_start+j;

						// Current real value
						float real = (float)x_start + (float)j_global * (float)dx;

						// Get actual real and imag value for curent iteration
						float zReal = (k == 0) ? real : preal_values[j];
						float zImag = (k == 0) ? imag : pimag_values[j];

						float r2 = zReal * zReal;
						float i2 = zImag * zImag;

						// Inverted stop condition
						if (r2 + i2 <= 4.0f)
						{
							// Accumulation of pixel value
							block_start_ptr[j] += 1;
						
							// Update values for next iteration
							pimag_values[j] = 2.0f * zReal * zImag + imag;
							preal_values[j] = r2 - i2 + real;
						}
						else
						{
							// Note finished items
							items_done += 1;
						}
					}

					// Iterations cycle can by stoped if all items are finished befor limit is reached
					if (items_done == block_width)
					{
						break;
					}
				}
			}
		}
	}
	return data;
}