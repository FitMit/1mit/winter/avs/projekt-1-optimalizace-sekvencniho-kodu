/**
 * @file LineMandelCalculator.cc
 * @author Matej Kudera <xkuder04@stud.fit.vutbr.cz>
 * @brief Implementation of Mandelbrot calculator that uses SIMD paralelization over lines
 * @date 17.10.2021
 */
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

#include <stdlib.h>

// mm_malloc
#include <immintrin.h>


#include "LineMandelCalculator.h"


LineMandelCalculator::LineMandelCalculator (unsigned matrixBaseSize, unsigned limit) :
	BaseMandelCalculator(matrixBaseSize, limit, "LineMandelCalculator")
{
	data = ((int *) _mm_malloc(height * width * sizeof(int), 64));
	real_values = ((float *) _mm_malloc(width * sizeof(float), 64));
	imag_values = ((float *) _mm_malloc(width * sizeof(float), 64));

	for (int i = 0; i < width * height; i++)
	{
		data[i] = 0;
	}
}

LineMandelCalculator::~LineMandelCalculator() {
	_mm_free(data);
	data = NULL;

	_mm_free(real_values);
	real_values = NULL;

	_mm_free(imag_values);
	imag_values = NULL;
}


int * LineMandelCalculator::calculateMandelbrot () 
{
	int *pdata = data;
	float *preal_values = real_values;
	float *pimag_values = imag_values;
	
	for (int i = 0; i < height; i++)
	{
		// Pointer to start of actual imag value line
		int *line_ptr = pdata + i*width;

		// Current imaginary value for whole line
		float imag = y_start + i * dy; 

		for (int k = 0; k < limit; k++)
		{
			// Items done for actual iteration
			int items_done = 0;

			#pragma omp simd simdlen(256) aligned(line_ptr, preal_values, pimag_values : 64) reduction(+:items_done)
			for (int j = 0; j < width; j++)
			{
				// Current real value
				float real = (float)x_start + (float)j * (float)dx; 

				// Get actual real and imag value for curent iteration
				float zReal = (k == 0) ? real : preal_values[j];
				float zImag = (k == 0) ? imag : pimag_values[j];

				float r2 = zReal * zReal;
				float i2 = zImag * zImag;

				// Inverted stop condition
				if (r2 + i2 <= 4.0f)
				{
					// Accumulation of pixel value
					line_ptr[j] += 1;

					// Update values for next iteration
					pimag_values[j] = 2.0f * zReal * zImag + imag;
					preal_values[j] = r2 - i2 + real;
				}
				else
				{
					// Note finished items
					items_done += 1;
				}
				
			}

			// Iterations cycle can by stoped if all items are finished befor limit is reached
			if (items_done == width)
			{
				break;
			}
		}
	}

	return data;
}
