#### Stručně odpovězte na následující otázky: ######

1. Proč je nutné určovat, zda má výpočet pokračovat?
==============================================================================
Výpočet komplexního čísla Zn se ukončuje hned po tom co je nalezena iterace, ve které se absolutní hodnota
tohoto čísla dostane přes hodnotu 2. Po této hodnotě se hodnoty komplexního čísla Zn rychle přibližují k nekonečnu, kdyby
jsme tedy počítali dále mohl by nastat problém s datovým typem float, ve kterém je tato hodnota uložena. Dle standartu 
IEEE je totiž chování přetečení typu float nedefinované.


2. Byla "Line" vektorizace efektivní? Proč?
==============================================================================
Nebyla z důvodu ukládání bloků hodnot do paměti cache. Tím že pro jednu iteraci procházíme vždy celý řádek, tak se nemusí
všechny hodnoty na řádku vlést do paměti cache a tedy prvky ke konci řádku nám vyhodí z cache bloky, které obsahují prvky na začátku řádku.
Při další iteraci je potom tedy znovu potřeba načíst bloky s prvky na začátku řádku, které zase z cache vyhodí bloky s hodnotami na konci řádku.


3. Jaká byla dosažena výkonnost v Intel Advisoru pro jednotlivé implementace 
(v GFLOPS)?
==============================================================================
Ref: 2.24 GFLOPS
Line: 34.70 GFLOPS
Batch: 36.27 GFLOPS

4. Jaká část kódu byla vektorizována v "Line" implementaci? Vyčteme tuto 
informaci i u batch kalkulátoru?
==============================================================================
V obou implementacích byla vektorizována nejzanořenější smyčka, neboli smyčka, která řeší procházení přes jednotlivé reálné hodnoty na řádku jedné konkrétní imaginární hodnoty.

5. Co vyčteme z Roofline modelu pro obě vektorizované implementace?
==============================================================================
Obě implementace se nachází na přelomu, který rozděluje programy, které jsou limitované výkonem a programy, které jsou limitované pamětí. Jediný rozdíl mezi implementacemi byl jak vysoko se nachazí.
"Line" vektorizace se nachází trochu pod čárou, která značí omezení pamětí L2 cahe a "Batch" vektorizace se oproti "line" nachází trochu nad touto čarou.


